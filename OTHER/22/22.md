http://tieba.baidu.com/p/3616451244

![ScreenShot](https://bitbucket.org/Dr_Newbie/tf2qna/raw/bb31269b36fa2c6819f0c20b890dcdfafd383c4a/OTHER/22/01.jpg)

－－－－－－－－－－－－－－－－－－－－－－－－－－

转自：komorebi ； http://t.cn/Rw870ur

下载：http://pan.baidu.com/s/1qWuRdiG ; [ALT](/OTHER/22/TransparentViewmodels.7z)

展示：http://www.bilibili.com/video/av2069319

－－－－－－－－－－－－－－－－－－－－－－－－－－

声明：

1. This method is simple hud-editing.
2. It shouldn't ever result in a VAC ban.
3. However: this might be considered an exploit as for some reason it affects some map textures as well, specifically spawn doors. This is disappointing as it increases the likelihood of this being patched in some way.
4. If used in conjunction with chris' or comanglia's maxframes fps configs it will work on all maps.
5. Right now it does not work on dx8, only dx9 (which sucks).
6. It should work on macintosh, not sure of linux.
7. Assuming the above conditions are met: it ~should~ work on all servers, sv_pure 2 or otherwise, at least until the workaround for custom materials is patched (if ever). Some public servers have broken all custom materials for me personally, usually ones running sourcemod to limit classes or disable capping on hightower. Someone in this thread mentioned skial badwater not working. It's possible there's some servermod that might render this ineffective, not sure.

－－－－－－－－－－－－－－－－－－－－－－－－－－

如何安装：

1. 把transparent.vpk 放到 tf\custom\ 底下。
2. 打开 \scripts\hudlayout.res 并把 HudLayout.txt 的内容贴到最底下 且有被 "Resource/HudLayout.res" 框住（http://t.cn/RwR2KFl）。
3. 打开autoexec.cfg 或是其他会自动被执行的cfg 并把Config Settings.txt的内容贴进去。