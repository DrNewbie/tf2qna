![ScreenShot](https://bitbucket.org/Dr_Newbie/tf2qna/raw/bb31269b36fa2c6819f0c20b890dcdfafd383c4a/OTHER/9/01.jpg)

以下是让你一劳永逸的转正方式

１、充钱，刷卡、Paypal、支付宝都行，充个5美金就够了

http://store.steampowered.com/steamaccount/addfunds

２、进商城买个东西，买什么都可以，再便宜都会让你转正

![ScreenShot](https://bitbucket.org/Dr_Newbie/tf2qna/raw/bb31269b36fa2c6819f0c20b890dcdfafd383c4a/OTHER/9/02.jpg)

![ScreenShot](https://bitbucket.org/Dr_Newbie/tf2qna/raw/bb31269b36fa2c6819f0c20b890dcdfafd383c4a/OTHER/9/03.jpg)

推荐方案是买壹把钥匙，因为钥匙可以换大量金属，金属可以换武器（参见交易入门）

![ScreenShot](https://bitbucket.org/Dr_Newbie/tf2qna/raw/bb31269b36fa2c6819f0c20b890dcdfafd383c4a/OTHER/9/04.jpg)

３、完成转正了，那我可以跟人交易了吗？

不

你还必须开启SteamGuard，并等候天数到期后才可以开始跟人交易

![ScreenShot](https://bitbucket.org/Dr_Newbie/tf2qna/raw/bb31269b36fa2c6819f0c20b890dcdfafd383c4a/OTHER/9/05.jpg)

４、我可以跟人交易了，那我可以去Steam市场买东西吗？

如果你是透过步骤３充值的人，那是的，但还是等天数到期

总之，使用市场的前提是要在Steam上有消费行为，然后等天数到期。

（附注：接受他人给你的游戏并不算数。）

Steam官方说明：

https://support.steampowered.com/kb_article.php?ref=6088-UDXM-7214

https://support.steampowered.com/kb_article.php?ref=6748-ETSG-5417

https://support.steampowered.com/kb_article.php?ref=1047-EDFM-2932