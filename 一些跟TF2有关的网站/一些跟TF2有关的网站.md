http://tieba.baidu.com/p/3883212593

##1. 讨论

军团要塞吧； 中国内最大的相关平台： <http://tieba.baidu.com/f?kw=军团要塞>

r/tf2： https://www.reddit.com/r/tf2/

r/truetf2： https://www.reddit.com/r/truetf2

TeamFortress TV： http://www.teamfortress.tv/

eXtv eSports： http://www.extelevision.com/

KritzKast： http://www.kritzkast.com/

steamcommunity/TF2： http://steamcommunity.com/app/440/discussions/

##2. 联赛

Asia Fortress； 亚洲： http://asiafortress.com/index.php/index.html

ETF2L； 欧洲： http://etf2l.org/

ESEA； 北美： https://news.esea.net/tf2/index.php

UGC；欧洲和北美： http://www.ugcleague.com/

##3. 交易

tf2交易吧： <http://tieba.baidu.com/f?kw=tf2交易>

tf2outpost： http://www.tf2outpost.com/

bazaar.tf： http://bazaar.tf/

backpack.tf； 提供物品历史，信誉，物价查询等多功能平台： http://backpack.tf/

Marketplace.tf； 现金买卖： https://marketplace.tf/

tf2shop； 现金买卖： http://tf2shop.net/

steamcommunity/Market； 现金买卖： http://steamcommunity.com/market/

##4. 服务

scrap.tf； 提供各种兑换服务，如武器换废金属，金属买钥匙、武器、皮肤、卡片、游戏等： https://scrap.tf/

tidy.tf； 提供武器和金属间的兑换以及回收服务： http://www.tidy.tf/

TF2B； 显示背包： https://tf2b.com/

tf2items； 显示背包和物品历史： https://tf2b.com/

config.tf； 帮助你完成设定文件： http://config.tf/

huds.tf； 便捷的HUD下载网站： http://huds.tf/

loadout.tf； 线上饰品搭配： http://loadout.tf/

weapons.tf； 线上武器皮肤预览： http://weapons.tf/

hudcreator.co.uk； 线上HUD产生器； http://tieba.baidu.com/p/4074027485

##5. 百科

Official Team Fortress Wiki； 官方百科，资讯和设定都可以在此找到： https://wiki.teamfortress.com/wiki/Main_Page

playcomp.tf； 竞技向指引： http://playcomp.tf/

comp.tf； 竞技向百科： http://comp.tf/wiki/Main_Page

##6. MOD\SKIN\MAP\...

gamebanana ; http://tf2.gamebanana.com/

TF2Maps.net ; http://tf2maps.net/

##7. 信誉

steamrep； 目前最大的信誉平台： http://steamrep.com/

rep.tf； 网罗各大平台的信誉： https://rep.tf/

Steam中国民间信誉查询： https://steamrepcn.com/ ； http://steamrep.sinaapp.com/ ; http://51rep.com/

##8. 菠菜

???

##9. 群组

Dr_Newbie开放群； 杂用： http://steamcommunity.com/groups/Newbaidu/

##10. bilibili

TF2Video： http://space.bilibili.com/956291

[SEARCH](https://search.bilibili.com/all?keyword=军团要塞2)