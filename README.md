本指引不解决任何智商问题

若欠缺阅读能力、不具备基本逻辑思考、电脑和网路知识严重贫乏等问题，请自行处理

－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－

跟我有关的Q&A：[LINK](/OTHER/ASK_ME/ASK_ME.MD)

－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－

1. 黑名单
2. 服务器列表
3. 如何添加服务器
4. Steam服务器有正常工作吗？（关系到商店\背包\装备\匹配等）
5. TF2安装和注册流程
6. 交易入门其一
7. 交易入门其二
8. 如何防骗
9. 怎么转成高级帐号（转正）？
10. 一些跟TF2有关的网站
11. 如何使用Scrap.tf
12. 为什么我进不了服务器？为什么突然断线？
13. 计算机屏蔽了VAC系统？( ... is blocking the VAC system ... )
14. 非安全模式
15. 如何建服
16. [模型资源]香蕉网的MOD资源转载
17. 如何安装HUD\自定义文件\MOD
18. 如何修复地图遗失（Missing Map）
19. 游戏备份
20. HUD
21. 依职业切换设定
22. 透明V模
23. win10 TF2不能启动（闪退）
24. config.tf ---- 帮助你完成设定文件
25. 终极低画质补丁三连弹，无帽+低画质cfg+超低贴图设置
26. 大量TF2设定与调整工具
27. 关於『Gun Mettle』更新后的崩溃问题
28. 如何显示伤害数字
29. 如何更改手臂长度\\枪的距离\\视野
30. 为什麼别人的介面跟我不一样
31. 在不是Windows的系统上的系统上，TF2的字会显示不全
32. 如何开启打击音效以及变更打击音效
33. 分辨率设定错误导致游戏无法执行
34. Linux缺字
35. 如何挑选低延迟的官服
36. 如何解锁黑玫瑰（Black Rose）
37. 尖叫要塞7更新 --- 问题与答案
38. out of memory
39. 【修复】Unable to load unicode.dll
40. 【教程】手机steam令牌
41. 说不定能解决你的死机问题
42. 有关备份还原、游戏主档案复制等
43. 那些有前坠名称或计数器的武器或帽子是什么？
44. 那些什么连杀是什么？（什么多少连杀、武器有特殊光泽、眼睛会喷火之类的）
45. XXX东西要怎么取得？
46. 物品掉落的规则是如何？
47. 『connection failed after 4 retries』？
48. 为什么主页面上出现[僵尸](/PICTURE/48-1.jpg)？
49. 东西上面写著[满月或节日才能使用](/PICTURE/48-2.png)是什么意思？
50. 如何邀请好友？
51. 创意工坊地图订阅后怎么玩？
52. 更新时硬盘满了？为什么更新一直胀大？为什么没有速度？
53. 什么样的配置才带的动本游戏？
54. 如何把更新前的创造服务器方式加回主页面上
55. 能不能玩单机\离线\跟BOT打？
56. BOT不会动？
57. BOT卡在一个地方？
58. 怎么加入BOT？
59. 如何开启控制台？
60. 皮肤\帽子\饰品有什么功用吗？
61. 为什么我下载时只有几GB，但之后变成几十多GB？
62. VPN？加速器？
63. 为什么停在100%？
64. 为什么那么慢？
65. 什么东西才能挂市场或与他人交易？
66. 如何还原种种设定？
67. 为什么一卡一卡的？
68. 怎么把语言改成中文？
69. 怎么修改游戏昵称？
70. 怎么跳舞＼击掌＼猜拳＼与他人互动？
71. MvM的门票怎么用？
72. 如何跟好友一起玩？
73. 屏幕超频？
74. 为什么我不能加好友？
75. 怎样才能开通Steam帐号＼取得更多功能？
76. Steam怎么绑定手机？
77. 怎么进入竞技模式？
78. 客户端崩溃
79. 怎么直接进入休闲模式的服务器？
80. 摄魂石像鬼（Soul Gargoyle）是做什么的？
81. 如何使用工作坊的地图

－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－

####1. 黑名单

[黑名单.md](/黑名单/黑名单.md)

####2. 服务器列表： 

[服务器列表.md](/服务器列表/服务器列表.md)

####3. 如何添加服务器： 

[如何添加服务器.md](/如何添加服务器/如何添加服务器.md)

####4. Steam服务器有正常工作吗？（关系到商店\背包\装备\匹配等）：

https://steamgaug.es/

https://steamstat.us/

####5. TF2安装和注册流程： 

http://tieba.baidu.com/p/2467997107

####6. 交易入门其一： 

http://tieba.baidu.com/p/3244893395

####7. 交易入门其二： 

http://tieba.baidu.com/p/3540242549

####8. 如何防骗： 

http://tieba.baidu.com/p/3748680197

[如何防骗.pdf](/如何防骗/【2015.05.09】如何防骗.pdf)

####9. 怎么转成高级帐号（转正）？

[9.md](/OTHER/9/9.md)

####10. 一些跟TF2有关的网站

[一些跟TF2有关的网站.md](/一些跟TF2有关的网站/一些跟TF2有关的网站.md)

####11. 如何使用Scrap.tf

[如何使用Scrap.tf.md](/如何使用Scrap.tf/如何使用Scrap.tf.md)

####12. 为什么我进不了服务器？为什么突然断线？

[为什么我进不了服务器_为什么突然断线.md](/为什么我进不了服务器_为什么突然断线/为什么我进不了服务器_为什么突然断线.md)

####13. 计算机屏蔽了VAC系统？( ... is blocking the VAC system ... )

[https://www.baidu.com/s?cl=3&wd=您的计算机屏蔽了VAC系统](http://t.im/18cis)

[https://www.baidu.com/s?cl=3&wd=is blocking the VAC system](http://t.im/18ciu)

http://tieba.baidu.com/p/3203217072

( 请先确认有没有被杀毒等其他软件阻挡 )

####14. 非安全模式

[非安全模式.md](/非安全模式/非安全模式.md)

####15. 如何建服：

http://tieba.baidu.com/p/3462039367

####16. [模型资源]香蕉网的MOD资源转载：

http://tieba.baidu.com/p/3187127934

####17. 如何安装HUD\自定义文件\MOD：

http://tieba.baidu.com/p/2490923051

####18. 如何修复地图遗失（Missing Map）： 

http://tieba.baidu.com/p/3605200278

http://steamcommunity.com/groups/NewbaiduTF2/discussions/0/617329920703587842/

####19. 游戏备份：

http://steamcommunity.com/groups/NewbaiduTF2/discussions/0/620713633863502775/

####20. HUD： 

http://huds.tf/

（ 如何更改文字的颜色、大小、字形： http://tieba.baidu.com/p/3783911189 ）

####21. 依职业切换设定： 

http://tieba.baidu.com/p/2532902712

####22. 透明V模： 

[9.md](/OTHER/22/22.md)

####23. win10 TF2不能启动（闪退）： 

http://tieba.baidu.com/p/3658948112

####24. config.tf ---- 帮助你完成设定文件： 

http://tieba.baidu.com/p/3715841678

####25. 终极低画质补丁三连弹，无帽+低画质cfg+超低贴图设置：

http://tieba.baidu.com/p/3561845286

####26. 大量TF2设定与调整工具： 

[大量TF2设定与调整工具.pdf](/大量TF2设定与调整工具/【2015.04.04】大量TF2设定与调整工具.pdf)

如果还是很有困难，更新配件/买新电脑才是上策

####27. 关於『Gun Mettle』更新后的崩溃问题： 

http://tieba.baidu.com/p/3875900634

####28. 如何显示伤害数字： 

http://t.cn/RqKQRh4

####29. 如何更改手臂长度\枪的距离\视野： 

[29.md](/OTHER/29/29.md)

####30. 为什麼别人的介面跟我不一样： 

[30.md](/OTHER/30/30.md)

####31. 在不是Windows的系统上的系统上，TF2的字会显示不全： 

[31.md](/OTHER/31/31.md)

####32. 如何开启打击音效以及变更打击音效： 

[32.md](/OTHER/32/32.md)

####33. 分辨率设定错误导致游戏无法执行： 

[33.md](/OTHER/33/33.md)

####34. Linux缺字： 

[http://t.cn/RuqKiXf](https://tieba.baidu.com/p/3806846209?pid=69389227567&cid=0#69389227567)

####35. 如何挑选低延迟的官服： 

http://tieba.baidu.com/p/3991457592 

http://t.cn/RLsQXFP

####36. 如何解锁黑玫瑰（Black Rose）： 

http://tieba.baidu.com/p/4018145952

####37. 尖叫要塞7更新 --- 问题与答案： 

http://tieba.baidu.com/p/4132961874

####38. out of memory： 

[http://t.cn/RuqKKhM](https://www.baidu.com/s?wd=out%20of%20memory)

如果你发现方法都试过了还是不行，那就代表你该升级配置了，再怎么说都是从2007更新到现在，
也差不多该汰旧换新了，像是作业系统升级到WIN7\WIN10且64位元，内存扩充到4GB以上等等等。

（简单说电脑不能太老、百病丛生）

（你也不要认为说用什么XX大师、XX优化，就能解决一切问题）

####39. 【修复】Unable to load unicode.dll： 

http://tieba.baidu.com/p/4244470418

http://tieba.baidu.com/p/4247797644

####40. 【教程】手机steam令牌： 

http://tieba.baidu.com/p/4232353134

####41. 说不定能解决你的死机问题： 

http://tieba.baidu.com/p/4336861892

####42. 有关备份还原、游戏主档案复制等

一般来说，我建议你使用Steam内建的备份和还原 http://tieba.baidu.com/p/2301894301

但如果你因为什么原因，或者发生明明有安装却显示没有安装这样的状况时，你可以试著在再一次选择安装到那个位置，也就是无视Steam跟你说需要重新下载，因为Steam下载安装时，是会扫说该位置的情况，所以有很大的可能是，Steam扫描后，发现你已经有了，于是验证个完整性，结束。

####43. 那些有前坠名称或计数器的武器或帽子是什么？

https://wiki.teamfortress.com/wiki/Strange

https://wiki.teamfortress.com/wiki/Strange_Part

https://wiki.teamfortress.com/wiki/Civilian_Grade_Stat_Clock

####44. 那些什么连杀是什么？（什么多少连杀、武器有特殊光泽、眼睛会喷火之类的）

https://wiki.teamfortress.com/wiki/Killstreak_Kit

####45. XXX东西要怎么取得？

这要看是什么东西，但一般来说，你可以靠和其他人做交易来取得

####46. 物品掉落的规则是如何？

https://wiki.teamfortress.com/wiki/Drop

你在有开启VAC保护的服务器内进行游戏就有机会拿到物品，至于频率、会拿到什么、以及为什么又没有拿到，一切一切都是系统决定，你不需要问，不需要不服气，如果你真的急着要东西，就去交易和商城。

####47. 『connection failed after 4 retries』？

简言之，你连不进去＼你断线了，至于原因是什么，很多，总归就是「这是你和服务器间的事情」，只能建议你换服务器（建议找低延迟的）或是试著改善你的网路品质。


####48. 为什么主页面上出现[僵尸](/PICTURE/48-1.jpg)？

https://wiki.teamfortress.com/wiki/Full_Moon

####49. 东西上面写著[满月或节日才能使用](/PICTURE/48-2.png)是什么意思？

https://wiki.teamfortress.com/wiki/Full_Moon

####50. 如何邀请好友？

[透过STEAM](/PICTURE/50-1.png)

####51. 创意工坊地图订阅后怎么玩？

https://steamcommunity.com/sharedfiles/filedetails/?id=454260231

or

http://t.cn/Rtm8WQs

####52. 更新时硬盘满了？为什么更新一直胀大？为什么没有速度？

- 现在硬盘都不贵，建议你直接入个几百G的或T的。 --->[硬盘](https://www.baidu.com/s?wd=硬盘) --->[移动硬盘](https://www.baidu.com/s?wd=移动硬盘)
* 一直胀大代表说你的档案被判定成「需要重新下载」，原因很多，可能你刚好就那么倒楣，可能你电脑有乱搞乱弄（然后你还不知道），也有可能他已经更新好了，但是无法执行后头的移动和清除，你可以自己用STEAM，下载，更新等关键字去搜寻。如果你要我出点子的话，我会建议你就放着给他更新，或是主动把一些东西删除，用验证完整性，又或者整个删干净，重新下载。
* 这问题广泛存在，你也不用太着急
- 因为没有下载，至于是因为你网路你跟你选的下载区不合，还是其他原因，不清楚，你可以用STEAM，下载，更新，缓慢等关键字去搜寻。

####53. 什么样的配置才带的动本游戏？

- 硬盘空间不能太小，你买个外接硬盘单独放你的STEAM游戏也行
- 你内存不能太小，随着游戏更新，消耗自然变大。 4G、8G都不贵，不要那么「省」，硬是用2G
- 你的CPU和显卡不能太差，文书用，太过古老或低端的不行，也不用追很高，你找个中价位的就OK，或是入二手
- 你的系统不能太乱，这算是个人电脑知识和习惯了，有些人就是要用什么XX大师、XX优化，然后装这个装那个，什么破解，什么免费，然后BOOM，系统乱七八糟
- 作业系统使用WIN7或WIN10，64位元，其他作业系统可以试试，但可能会有些奇怪的现象

其实简单说就是该更新，该维护时就要做，我个人对我的六年多的电脑做了一些维护和升级后，现在游戏都很顺，虽然得花钱，但考虑到已经'六年'多了，也没有贵到哪去。

####54. 如何把更新前的创造服务器方式加回主页面上

如果你不喜欢用［控制台 - MAP <地图名称>］这种开图方式，你可以自己把它加回去

http://tieba.baidu.com/p/4661904998

####55. 能不能玩单机\离线\跟BOT打？

你随时都可以用指令［map "地图的名称"］开启任何你有的地图

或是： http://tieba.baidu.com/p/4661904998

####56. BOT不会动？

https://wiki.teamfortress.com/wiki/Bots/zh-hant

####57. BOT卡在一个地方？

https://wiki.teamfortress.com/wiki/Bots/zh-hant

终究是系统自动算出来的路径，可能会跟实际地图路线不符

####58. 怎么加入BOT？

tf_bot_add

https://developer.valvesoftware.com/wiki/List_of_TF2_console_commands_and_variables

####59. 如何开启控制台？

[如何开启控制台.md](/OTHER/如何开启控制台/如何开启控制台.md)

####60. 皮肤\帽子\饰品有什么功用吗？

就是皮肤\帽子\饰品，没其他特别的能力

####61. 为什么我下载时只有几GB，但之后变成几十多GB？

你不知道「档案压缩」吗？

####62. VPN？加速器？

依然是你和服务器间的事情

请有点常识，不是开了就能解决，问题是『这些东西和你连的好不好』以及『提供这服务的与目标服务器连的好不好』，因此『你<--->VPN或加速器<--->游戏服务器』这三者畅通，你才会畅通。

人们有时直接连线『你<--->游戏服务器』会得到很差甚至无法连上的状况，所以希望『VPN或加速器<--->游戏服务器』能帮你连好，然后传给你稳定的连线『你<--->VPN或加速器』，提升游戏品质。

你也不用特别开帖问，你可以去找免费的，你可以去找有鉴赏期的，而实际如何，要你自己试了才知道。

（类推到访问各种网站）

####63. 为什么停在100%？

因为还需要解压缩、还需要做一些事情

####64. 为什么那么慢？

撇除当机，你电脑差( ---> [#53](http://t.cn/RuqK8bf) )，若你有兴趣，可以去充实相关电脑基本知识

你不用跟我们争说你的有多好多强花了多少钱，现况就是你跑不动

####65. 什么东西才能挂市场或与他人交易？

首先，你的帐号要能挂市场或与他人交易。

接著，如果那东西能挂市场或与他人交易，那你就能在进行相关操作时，看到那东西。

如果你非要个明确点的答案，1，那东西的起源有实际金钱根据，2，V社设定说这能挂市场或与他人交易。

你不用太奢想说这游戏会免费送大钱给你，纯粹免费掉落的且可以挂市场的，往往是［大家都有的便宜货］，所以，请自行参考交易入门内的查价。

####66. 如何还原种种设定？

1. 进入你的［common\Team Fortress 2\tf］
2. 删除这些文件夹［cfg］、［custom］、［download］
3. 验证完整性

####67. 为什么一卡一卡的？

撇除你电脑差，大概是因为输入法

####68. 怎么把语言改成中文？

你的Steam改成中文

http://steamcn.com/t43618-1-1

####69. 怎么修改游戏昵称？

修改Steam的昵称

####70. 怎么跳舞＼击掌＼猜拳＼与他人互动？

站在正确的位置，并按下嘲讽按键

简单的范例 : http://t.cn/RVi75SU

####71. MvM的门票怎么用？

你有门票才能进官方的MvM，才会有奖品，在你过关前都不会用掉

简单说，你不用管那么多，门票在，你可以进，门票没了，但你还想继续，就要去入手

####72. 如何跟好友一起玩？

进同一个服务器，你丢IP给他，他丢IP给你，你邀请他，他邀请你

官服匹配？跟他组队

两人一个在北极一个在南极？你们其中一方飞去另一方，或是两人到中央

####73. 屏幕超频？

[33.md](/OTHER/33/33.md)

####74. 为什么我不能加好友？

你是受限制的帐号

详阅：[support.steampowered.com/kb_article.php?ref=3330-IAGK-7663](http://t.cn/RZjCpPY)

####75. 怎样才能开通Steam帐号＼取得更多功能？

你需要在Steam上面消费满五美金

详阅：[support.steampowered.com/kb_article.php?ref=3330-IAGK-7663](http://t.cn/RZjCpPY)

* Adding the equivalent of $5 USD or more to your [Steam Wallet](http://t.cn/RuqK1mp)
* Purchasing game(s) that are equal to $5 USD or more from the [Steam store](http://t.cn/RxDH4xz)
* Adding a [Steam Wallet card](http://t.cn/8kJtXq0) that is equal to $5 USD or more to your Steam account
* Purchasing a [Steam gift](http://t.cn/RuqKFTk) that is equal to $5 USD or more from the Steam store (Receiving a Steam gift from a friend doesn't count)

####76. Steam怎么绑定手机？

http://t.cn/Ruq97Nm

####77. 怎么进入竞技模式？

需要是高级帐号（也就是转正）

绑定手机

或购买通行证 http://t.cn/RVi7jv0

####78. 客户端崩溃： 

http://tieba.baidu.com/p/3212671653

####79. 怎么直接进入休闲模式的服务器？

不能

####80. 摄魂石像鬼（Soul Gargoyle）是做什么的？

[80.md](/OTHER/80/80.md)

####81. 如何使用工作坊的地图

[如何使用工作坊的地图](https://steamcommunity.com/sharedfiles/filedetails/?id=1084116596)
